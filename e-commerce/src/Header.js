import React from "react";
import "./Header.css";
import { Link } from "react-router-dom";
import logo from "./images/logo-new-edit.png";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import { useStateValue } from "./StateProvider";
import { auth } from "./firebase";

function Header() {
  const [{ cart, user }] = useStateValue();

  const login = () => {
    if (user) {
      auth.signOut();
    }
  };

  console.log(cart);

  return (
    <nav className="header">
      <div className="header__menuToggle">
        <input type="checkbox" />
        <span></span>
        <span></span>
        <span></span>

        <ul className="header__menuToggleMenu">
          <li>
            <strong className="header__menuToggleCategory">Category</strong>
          </li>
          <Link to="" className="header__toggleLink">
            <li>Food</li>
          </Link>
          <Link to="" className="header__toggleLink">
            <li>Fashion</li>
          </Link>
        </ul>
      </div>

      <Link to="/">
        <img className="header__logo" src={logo} alt="" />
      </Link>

      <div className="header__search">
        <input type="text" className="header__searchInput" />
        <SearchIcon className="header__searchIcon" />
      </div>

      <div className="header__nav">
        <Link to={!user && "/login"} className="header__linK">
          <div onClick={login} className="header__option">
            <span className="header__optionLineOne">
              Hello, {user?.email.replace(/@.*/, "")}
            </span>
            <span className="header__optionLineTwo">
              {user ? "Sign Out" : "Sign In"}
            </span>
          </div>
        </Link>

        <Link to="" className="header__linK">
          <div className="header__option">
            <span className="header__optionLineOne">Find Out</span>
            <span className="header__optionLineTwo">About Us</span>
          </div>
        </Link>

        <Link to="/checkout" className="header__linK">
          <div className="header__optionCart">
            <ShoppingCartOutlinedIcon />
            <span className="header__optionLineTwo header__cartCount">
              {cart?.length}
            </span>
          </div>
        </Link>
      </div>
    </nav>
  );
}

export default Header;
