import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBv1TzXFi6oqcvS8jBFUfiCqwy4bUM3EAM",
  authDomain: "e-commerce-bbe34.firebaseapp.com",
  databaseURL: "https://e-commerce-bbe34.firebaseio.com",
  projectId: "e-commerce-bbe34",
  storageBucket: "e-commerce-bbe34.appspot.com",
  messagingSenderId: "124553247757",
  appId: "1:124553247757:web:6eeccc27e2f331bd30a944",
  measurementId: "G-BQ27P498L9"
});

const auth = firebase.auth();

export { auth };
