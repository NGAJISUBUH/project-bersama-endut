import React from "react";
import headerImage from "./images/header.jpg";
import "./Home.css";
import Product from "./Product";
import Slider from "./Slider"

function Home() {
  return (
    <div className="home">
      <Slider/>
      {/* <img className="home__image" src={headerImage} alt="" /> */}
      <div className="home__row">
        <Product
          id="12345678"
          title="Crayola Colored Pencil Set, School Supplies, Assorted Colors, 36 Count"
          price={6.86}
          rating={5}
          image="https://images-na.ssl-images-amazon.com/images/I/81Vje4dOoKL._AC_SL1500_.jpg"
        />
        <Product
          id="12345679"
          title="Apple iPad (10.2-inch, Wi-Fi, 32GB) - Space Gray (Latest Model)"
          price={427.99}
          rating={4}
          image="https://images-na.ssl-images-amazon.com/images/I/6162WMQWhVL._AC_SL1500_.jpg"
        />
      </div>

      <div className="home__row">
        <Product
          id="12345610"
          title="AMD Ryzen 7 3700X 8-Core"
          price={331.99}
          rating={5}
          image="https://images-na.ssl-images-amazon.com/images/I/717Di3DGIbL._AC_SL1092_.jpg"
        />
        <Product
          id="12345611"
          title="Blendtec Total Classic Original Blender - WildSide+ Jar (90 oz) - Professional-Grade Power"
          price={615.99}
          rating={4}
          image="https://images-na.ssl-images-amazon.com/images/I/31znGMx%2B9FL._AC_.jpg"
        />
        <Product
          id="12345612"
          title="Keurig K-Mini Coffee Maker, Single Serve K-Cup Pod Coffee Brewer"
          price={96.99}
          rating={4}
          image="https://images-na.ssl-images-amazon.com/images/I/71tybZyMkuL._AC_SL1500_.jpg"
        />
      </div>

      <div className="home__row">
        <Product
          id="12345613"
          title="Nintendo Switch with Neon Blue and Neon Red Joy‑Con"
          price={556.99}
          rating={5}
          image="https://images-na.ssl-images-amazon.com/images/I/61JnrafZ7zL._AC_SL1457_.jpg"
        />
      </div>
    </div>
  );
}

export default Home;
