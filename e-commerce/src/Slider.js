import React, { useState } from "react";
import "./Slider.css";
import ImgComp from "./ImgComp";
import i1 from "./images/header_pic4.jpg";
import i2 from "./images/header_pic3.jpg";
import i3 from "./images/header_pic2.jpg";
import i4 from "./images/header_pic5.jpg";
import i5 from "./images/header_pic6.jpg";

function Slider() {
  const [x, setX] = useState(0);

  // Array for component to show inside the slider
  let sliderArr = [
    <ImgComp src={i1} />,
    <ImgComp src={i2} />,
    <ImgComp src={i3} />,
    <ImgComp src={i4} />,
    <ImgComp src={i5} />,
  ];
  const goLeft = () => {
    console.log(x);
    // dynamic carousel logic
    x === 0 ? setX(-100 * (sliderArr.length - 1)) : setX(x + 100);
  };
  const goRight = () => {
    console.log(x);
    // dynamic carousel logic
    x === -100 * (sliderArr.length - 1) ? setX(0) : setX(x - 100);
  };

  return (
    <div className="slider">
      {sliderArr.map((item, index) => {
        return (
          <div
            key={index}
            className="slide"
            style={{ transform: `translateX(${x}%)` }}
          >
            {item}
          </div>
        );
      })}

      <button id="goLeft" onClick={goLeft}>
        <i class="fa fa-chevron-left fa-3x" aria-hidden="true"></i>
      </button>
      <button id="goRight" onClick={goRight}>
        <i class="fa fa-chevron-right fa-3x" aria-hidden="true"></i>
      </button>
    </div>
  );
}

export default Slider;
