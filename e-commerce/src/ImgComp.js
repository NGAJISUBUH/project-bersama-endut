import React from "react";
import "./ImgComp.css";

function ImgComp({ src }) {
  return <img className="imgComp" src={src} alt="slide-img"></img>;
}

export default ImgComp;
